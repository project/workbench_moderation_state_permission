-- INTRODUCTION --

Workbench Moderation State Permission add permissions 
in order to control access on content that moderated 
using module Workbench Moderation.

-- REQUIREMENTS --
 
Drupal 7.x : Workbench Moderation.


-- INSTALLATION --

1. Copy the entire workbench_moderation_state_permission directory 
the Drupal sites/all/modules directory.

2. Login as an administrator. Enable the module in the "Administer" -> "Modules"

3. Should exist at least one content type enabled in Workbench Moderation.

4. Rebuild permissions "Administration » Reports" 
in section "Node Access Permissions"

5. Check all permission that added in Administration » people » permissions.


-- CONFIGURATION -
There is no configuration. When
enabled, open Administration » people » permissions search the name group 
"Workbench Moderation State Permission" 
and check the permission that you needed.
